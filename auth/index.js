import consola from 'consola'
import parseBasicAuth from 'basic-auth'

const bodyStyles = `
margin: 0;
width: 100vw;
height: 100vh;
display: flex;
flex-direction: column;
justify-content: center;
align-items: center; 
`

const themeName = ''
const theme = themeName && `<link rel="stylesheet" href="https://jenil.github.io/bulmaswatch/${themeName}/bulmaswatch.min.css">`
const logoutPage = (login, message, error) => {
  return `<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>${message}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css">
    ${theme}
  </head>
  <body style="${bodyStyles}">
    <div class="notification subtitle ${error && 'is-danger'}">${message}</div>
    <a href="/" class="button ${!error && 'is-info'}">${login}</a>
  </body>`
}

export default function ({ auth }, nuxt) {
  const { user, pass, enabled, realm } = auth

  if (enabled === false) {
    consola.info('Authorization disabled (config)')
    return
  }

  if (!user || !pass) {
    consola.error('Authorization disabled (missing credentials)')
    return
  }

  nuxt.addServerMiddleware(function (req, res, next) {
    if (req.url === '/logout') {
      res.statusCode = 401
      return res.end(logoutPage('Zaloguj ponownie', 'Wylogowano pomyślnie'))
    }

    const credentials = parseBasicAuth(req)
    if (!credentials || credentials.name !== user || credentials.pass !== pass) {
      res.statusCode = 401
      res.setHeader('WWW-Authenticate', `Basic realm="${realm || 'Please enter username and password'}"`)
      return res.end(logoutPage('Zaloguj', 'Błąd autoryzacji', true))
    }

    if (req.url === '/db' || req.url.startsWith('/db/')) {
      delete req.headers.authorization
    }

    return next()
  })

  consola.success('Authorization enabled')
}
