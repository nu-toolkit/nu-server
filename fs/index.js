import consola from 'consola'
import axios from 'axios'
import express, { json } from 'express'
// import merge from 'lodash/merge'
// import fs from 'fs-extra'
import fileUpload from 'express-fileupload'
import request from 'request-promise-native'
import { generateId } from '../utils'

const app = express()

// const defaultOptions = {

// }

const getUploadSignature = ({ apiKey, privateKey, filename, timestamp }) => {
  const serialisedString = `apiKey=${apiKey}&filename=${filename}&timestamp=${timestamp}`
  const uploadSignature = require('create-hmac')('sha1', privateKey)
    .update(serialisedString)
    .digest('hex')

  return uploadSignature
}

const getDeleteSignature = ({ imagekitId, imagePath, privateKey }) => {
  const serialisedString = `imagekitId=${imagekitId}&path=${imagePath}`
  const deleteSignature = require('create-hmac')('sha1', privateKey)
    .update(serialisedString)
    .digest('hex')

  return deleteSignature
}

const formUrlEncoded = x =>
  Object.keys(x).reduce((p, c) => p + `&${c}=${encodeURIComponent(x[c])}`, '')

export default async function({ fs }, nuxt) {
  const { enabled, provider, imagekitId, apiKey, privateKey } = fs

  if (enabled === false) {
    consola.info('Filesystem disabled (config)')
    return
  }

  if (!provider || !imagekitId || !apiKey || !privateKey) {
    consola.info('Filesystem disabled (configuration missing)')
    return
  }

  // app.post('/uploadTest', fileUpload(), function(req, res) {
  //   console.log(req.files, req.body)
  // })

  app.post('/upload', fileUpload(), async function(req, res) {
    // if (!acceptedMimetypes[file.mimetype]) {
    //   return res.status(500).send({ error: 'Wrong file extension' })
    // }
    try {
      const { file } = req.files

      const timestamp = new Date().getTime()
      const filename = generateId()

      const signature = getUploadSignature({
        apiKey,
        privateKey,
        filename,
        timestamp
      })

      var formData = {
        image: {
          value: file.data,
          options: {
            filename: req.files.file.name
          }
        },
        imagekitId,
        apiKey,
        filename,
        timestamp,
        signature
      }

      const response = await request.post({ url: 'http://upload.imagekit.io/rest/api/image/' + imagekitId, formData })

      const { imagePath, width, height, url, thumbnail } = JSON.parse(response)

      res.send({
        imagePath,
        width,
        height,
        url,
        thumbnail
      })
    } catch (err) {
      res.status(500).send({ error: true, message: 'Internal server error.' })
      throw err
    }
  })

  app.post('/uploadSignature', json(), async(req, res) => {
    const { filename } = req.body
    if (!filename) return res.send({ error: true, message: 'Missing file name.' })

    const timestamp = new Date().getTime()

    const uploadSignature = getUploadSignature({
      apiKey,
      privateKey,
      filename,
      timestamp
    })

    return res.send({ timestamp, uploadSignature, apiKey })
  })

  app.post('/delete', json(), async(req, res) => {
    const { imagePath } = req.body
    if (!imagePath) return res.send({ error: true, message: 'Missing image path.' })

    try {
      const deleteSignature = getDeleteSignature({
        imagekitId,
        imagePath,
        privateKey
      })

      const { data } = await axios(
        'https://imagekit.io/api/admin/media/deleteFile',
        {
          method: 'post',
          data: formUrlEncoded({
            imagekitId,
            path: imagePath,
            signature: deleteSignature
          })
        }
      )

      return res.send(data)
    } catch (err) {
      res.send({ error: true, message: 'Internal server error.' })
      throw err
    }
  })

  consola.success(`Filesystem enabled (${provider})`)

  nuxt.addServerMiddleware({
    path: '/fs',
    handler: app
  })
}
