import PouchDB from 'pouchdb-core'
import adapterHttp from 'pouchdb-adapter-http'
import mapReduce from 'pouchdb-mapreduce'
import replication from 'pouchdb-replication'
import find from 'pouchdb-find'
import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import Buefy from 'buefy'

import 'buefy/dist/buefy.css'
import 'material-icons/iconfont/material-icons.css'

import { getDbName } from './utils'
import components from './components'

const registerComponents = {
  install(Vue) {
    Object.keys(components).forEach(name => {
      Vue.component(`nu-${name}`, components[name])
    })
    Vue.use(Buefy)
  }
}

PouchDB.plugin(adapterHttp)
PouchDB.plugin(mapReduce)
PouchDB.plugin(replication)
PouchDB.plugin(find)

const port = window.location.port ? ':' + window.location.port : ''
const host = `${window.location.protocol}//${window.location.hostname}${port}`

const initCollections = (configDb) => {
  const { collections, prefix, postfix } = configDb

  const dbHost = `${host}/db/`

  const dbs = {}

  return name => {
    if (!name) throw Object({ error: true, msg: 'Missing collection name' })

    const idx = collections.indexOf(name)
    if (idx === -1) throw Object({ error: true, msg: 'Missing collection' })

    if (!dbs[name]) dbs[name] = new PouchDB(dbHost + getDbName({ name, prefix, postfix }))

    return dbs[name]
  }
}

const initFs = ({ imagekitId }) => {
  return {
    imagekitId,
    deleteFile(imagePath) {
      return axios(
        host + '/fs/deleteFile',
        {
          method: 'post',
          data: {
            imagePath
          }
        }
      ).then(({ data }) => data)
        .catch(console.log)
    }
  }
}

Vue.use(Vuex)
const initI18nStore = ({ langs, current }) => {
  if (!langs || langs.length < 1) return
  return new Vuex.Store({
    state: {
      langs: langs,
      current: current || langs[0]
    },
    mutations: {
      set(state, lang) {
        if (state.langs.includes(lang)) {
          state.current = lang
        }
      }
    }
  })
}

export { registerComponents, initCollections, initFs, initI18nStore }
