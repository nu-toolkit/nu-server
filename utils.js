import { int_to_base58 as intToBase58 } from './base58'

const getDbName = ({ prefix, postfix, name }) => {
  if (prefix && postfix) return `${prefix}-${name}-${postfix}-collection`
  else if (prefix && !postfix) return `${prefix}-${name}-collection`
  else if (!prefix && postfix) return `${name}-${postfix}-collection`
  else return `${name}-collection`
}

const generateId = (timestamp) => {
  return intToBase58(timestamp || new Date().getTime())
}

export default {
  getDbName,
  generateId
}

export {
  getDbName,
  generateId
}
