const pouch = require('pouchdb-core')
pouch.plugin(require('pouchdb-replication'))
pouch.plugin(require('pouchdb-mapreduce'))
pouch.plugin(require('pouchdb-find'))

export function nodePouch ({ adapter, path }) {
  if (adapter === 'memory') pouch.plugin(require('pouchdb-adapter-memory'))
  if (adapter === 'leveldb') pouch.plugin(require('pouchdb-adapter-leveldb'))

  return pouch.defaults({
    adapter: adapter,
    prefix: path + '/'
  })
}

export function httpPouch ({ source }) {
  const pouch = require('pouchdb-core')

  pouch.plugin(require('pouchdb-adapter-http'))

  return pouch.defaults({
    adapter: 'http',
    prefix: source + '/'
  })
}
