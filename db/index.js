import consola from 'consola'
import express from 'express'
import merge from 'lodash/merge'
import { nodePouch, httpPouch } from './pouch'
import { getDbName } from '../utils'
import ora from 'ora'
import expressPouch from './expressPouch'

import nodePath from 'path'
import { get } from 'https'

const app = express()

const defaultOptions = {
  enabled: true,
  adapter: 'leveldb',
  path: './tmp/db',
  prefix: 'nu',
  development: {
    sync: 'pull',
    postfix: 'dev'
  },
  production: {
    sync: 'full'
  }
}

const getOptions = ({ db }) => {
  const { development = {}, production = {}, ...mainOptions } = merge(defaultOptions, db)
  const enviroments = { development, production }

  const all = merge(mainOptions, enviroments[process.env.NODE_ENV] || {})

  return all
}

const syncDbs = async({ source, dbs, sync }) => {
  const RemotePouch = httpPouch({ source })
  const spinner = ora(`Database pulling`).start()
  const promises = dbs.map((localDb, idx) => new Promise(async (resolve, reject) => {
    const { name } = localDb
    const remoteDb = new RemotePouch(name.replace('-dev', ''))
    
    // console.log(localDb.name, remoteDb.name)
    
    await new Promise(resolve => setTimeout(resolve, idx * 1000)) // FIXME: needs limiter

    localDb.replicate.from(remoteDb)
      .on('complete', (info) => {
        if (sync === 'full') localDb.sync(remoteDb, { live: true, retry: true })
        // spinner.stop()
        return resolve(info)
      })
      .on('error', (error) => {
        // spinner.fail(`Failed to pull collection ${name}`)
        throw error
      })
  }))

  await Promise.all(promises)

  if (sync === 'full') spinner.succeed('Database pulled and syncing')
  else spinner.succeed('Database pulled')
}

const init = async function({ auth, db }, nuxt) {
  const { enabled, source, adapter, path, sync, collections, prefix, postfix } = getOptions(db)

  if (enabled === false) {
    consola.info('Database disabled (config)')
    return
  }

  if (!collections || collections.length === 0) {
    consola.info('Database disabled (no collections configured)')
    return
  }

  const Pouch = nodePouch({ adapter, path })

  app.use('/db', await expressPouch({ adapter, path, Pouch }))

  const dbs = collections.map(name => new Pouch(getDbName({ prefix, postfix, name })))

  // consola.info('Database collections:', collections.join(', '))

  if (!sync) {
    consola.info('Database sync disabled (config)')
  }

  if (sync && !source) {
    consola.info('Database sync disabled (no source configured)')
  }

  if (source && (sync === 'full' || sync === 'pull')) {
    await syncDbs({ source, dbs, sync })
  }

  nuxt.addServerMiddleware({
    path: '/',
    handler: app
  })
}

export default {
  getOptions,
  init
}
