import fs from 'fs-extra'
import { nodePouch } from './pouch'
import expressPouchDB from 'express-pouchdb'

export default async ({ adapter, path, Pouch }) => {
  await fs.ensureDir(path)

  const epd = expressPouchDB(Pouch, {
    logPath: path + '/log.txt',
    inMemoryConfig: true
  })

  // TODO: find a way to configure users and permissions
  // if (auth.user && auth.pass && auth.enable !== false) {
  //   epd.couchConfig.set('admins', 'test', 'xxx', () => {})
  //   epd.couchConfig.set('chttpd', 'require_valid_user', true, () => {})
  //   console.log(epd.couchConfig)
  // }

  return epd
}
