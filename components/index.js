import icon from './icon.vue'
import editor from './editor.vue'
import wysiwyg from './wysiwyg.vue'
import langSwicher from './langSwicher.vue'
import translate from './translate.vue'
import file from './file'

export default {
  icon,
  editor,
  wysiwyg,
  langSwicher,
  translate,
  file
}
