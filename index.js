import consola from 'consola'
import getAuth from './auth'
import db from './db'
import getFs from './fs'
import nodePath from 'path'

module.exports = async function(moduleOptions) {
  const options = Object.assign({}, this.options.nu, moduleOptions || {})
  const nuxt = this

  // TODO: find better way
  nuxt.addPlugin({
    src: nodePath.resolve(__dirname, './plugin.js'),
    options: {
      db: db.getOptions(options),
      i18n: options.i18n,
      fs: options.fs
    }
  })
  consola.success('Client plugin attached')

  if (process.env.npm_lifecycle_script !== 'nuxt build') {
    getAuth(options, nuxt)

    await db.init(options, nuxt)

    await getFs(options, nuxt)
  }

  // process.exit()
}

module.exports.meta = require('./package.json')
