import Vue from 'vue'
import { registerComponents, initCollections, initFs, initI18nStore } from 'nu-server/browser'

Vue.use(registerComponents)

const options = <%= JSON.stringify(options, null, 2) %>

export default ({ app }, inject) => {
  inject('db', initCollections(options.db))
  inject('fs', initFs(options.fs))
  inject('i18n', initI18nStore(options.i18n))
}
